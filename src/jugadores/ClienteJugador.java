package jugadores;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;


/**
 * Establece la conexión por la parte del Cliente/Jugador.
 *
 * @author Bruno Núñez Manuel   1ºDAM DUAL B
 * @version 1.0
 * @since 26/03/2021
 */
public class ClienteJugador {

    /**
     * Conecta el cliente con el servidor, estableciendo un protocolo de
     * comunicación de mensajes intercambiados secuencialmente uno a uno:
     * envío-respuesta-envío-respuesta etc.
     * La comunicación por parte del cliente se cierra cuando el servidor
     * envía un mensaje terminado en 'ADIÓS'.
     */
    public static void conectaCliente() {
        Scanner scanner = new Scanner(System.in);
        try {
            Socket socket = new Socket("localhost", 12345);
            DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());

            while (true) {
                String mensajeRecibido = dataInputStream.readUTF();
                System.out.println(mensajeRecibido);
                if (mensajeRecibido.endsWith("ADIÓS")) break;

                String respuestaJugador = scanner.nextLine();
                dataOutputStream.writeUTF(respuestaJugador);
            }

            socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
