package servidor;

import javax.sound.sampled.*;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;


/**
 * Para tu goce...
 */
public class IntroLoca extends JFrame {
    ImageIcon image;
    Dimension imageDimension;
    JLabel imageLabel;
    //
    JLayeredPane layeredPane;
    JPanel buttonsPanel;
    JButton play;
    JButton exit;
    //
    AudioInputStream audioInputStream;
    Clip clip;

    /**
     * El propio nombre lo indica.
     */
    public IntroLoca() {
        super();

        try {
            image = new ImageIcon("src/media/imageIntro.png");
            imageDimension = new Dimension(image.getIconWidth(), image.getIconHeight());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            this.audioInputStream = AudioSystem.getAudioInputStream(
                    new File("src/media/19th Floor - Bobby Richards.wav").getAbsoluteFile());
            AudioFormat format = audioInputStream.getFormat();
            DataLine.Info info = new DataLine.Info(Clip.class, format);
            clip = (Clip) AudioSystem.getLine(info);
            clip.open(audioInputStream);
            clip.start();
            clip.loop(99);
        } catch (IOException | LineUnavailableException | UnsupportedAudioFileException e) {
              e.printStackTrace();
        }

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new GridLayout());
        this.setSize(new Dimension(
                (int)imageDimension.getWidth(), (int)(imageDimension.getHeight() + 100)));
        this.setTitle("TRIVIAL");
        addLayeredPane();
        this.pack();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setVisible(true);
    }

    /**
     * Les botones.
     */
    void addLayeredPane() {
        layeredPane = new JLayeredPane();
        layeredPane.setLayout(new BorderLayout());
        layeredPane.setPreferredSize(this.getSize());

        imageLabel = new JLabel(image);
        imageLabel.setSize(imageDimension);
        layeredPane.add(imageLabel,0);

        buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout());
        buttonsPanel.setPreferredSize(new Dimension(300,100));

        play = new JButton("PLAY");
        play.setFont(new Font("Consolas", Font.BOLD, 50));
        play.addActionListener(e -> {
            this.setVisible(false);
            JOptionPane.showMessageDialog(null,
                    """
                            Siento decepcionarte, pero vas a tener que jugar con la terminal.
                            Me ha tentado hacerlo todo en Swing pero entonces tendría que cobrarte
                            por esta obra de arte.""");
        }); // Más TextBlocks, qué placer...

        exit = new JButton("EXIT");
        exit.setFont(new Font("Consolas", Font.BOLD, 50));
        exit.addActionListener(e -> System.exit(0));

        buttonsPanel.add(play);
        buttonsPanel.add(exit);
        //
        layeredPane.add(buttonsPanel, BorderLayout.SOUTH,1);
        //
        this.add(layeredPane);
    }

}
