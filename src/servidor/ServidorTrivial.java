package servidor;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Semaphore;


/**
 * El servidor. Creará nuevos hilos para cada conexión establecida con
 * un cliente. Al tener un Semaphore de 2 permisos, cuya referencia es
 * transferida a cada hilo creado, únicamente dos clientes podrán jugar
 * en sus correspondientes hilos a la vez.
 * @author Bruno Núñez Manuel   1ºDAM DUAL B
 * @since 26/03/2021
 * @version 1.0
 */
public class ServidorTrivial {

    public static void main(String[] args) {
        try {
            // new IntroLoca(); // a lo mejor se hace un poco pesada...
            Semaphore semaphore = new Semaphore(2);
            ServerSocket servidor = new ServerSocket(12345);
            System.out.println("Servidor iniciado.");

            while (true) {
                Socket socket = servidor.accept();
                new HiloServidorTrivial(socket, semaphore).start();
                System.out.println("Jugador conectado");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
