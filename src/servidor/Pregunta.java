package servidor;

import java.util.Arrays;
import java.util.Collections;

/**
 * Clase Pregunta para disponer de cada enunciado, sus opciones, la correcta etc.
 * @author Bruno Núñez Manuel   1ºDAM DUAL B
 * @since 18/03/2021
 * @version 1.0
 */
public class Pregunta {
    private final String enunciado;
    private final String[] opciones;
    char correcta;

    /**
     * @param enunciado la pregunta en sí.
     * @param opciones sus posibles respuestas.
     */
    public Pregunta(String enunciado, String[] opciones) {
        this.enunciado = enunciado;
        this.opciones = opciones;
        mezclarInCrUsTaRLetrasYsetearCorrecta();
    }

    /**
     * Desordena 'aleatoriamente' (o casi) las opciones de cada pregunta, les añade
     * una letra y setea la respuesta correcta.
     */
    private void mezclarInCrUsTaRLetrasYsetearCorrecta() {
        // Este método es aberrante... No me gusta.
        // Espero que separar la funcionalidad del método requerido imprimePregunta()
        // en mezclarInCrUsTaRLetrasYsetearCorrecta() no implique mi destrucción y muerte.
        String stringCorrecta = opciones[0];
        Collections.shuffle(Arrays.asList(opciones));
        // Añado letras a las opciones...
        opciones[0] = "a). " + opciones[0];
        opciones[1] = "b). " + opciones[1];
        opciones[2] = "c). " + opciones[2];
        opciones[3] = "d). " + opciones[3];
        // Encuento la letra de la opción correcta y la returno:
        // me siento raro, algo no encaja...
        for (String opcion : opciones) {
            if (opcion.contains(stringCorrecta)) {
                this.correcta = opcion.charAt(0);
            }
        }
    }

    /**
     * @return letra de la respuesta correcta convertida a String.
     */
    public String getCorrecta() {
        // Merece la pena que this.correcta sea un char en vez de un String?
        return String.valueOf(correcta);
    }


    /**
     * @return enunciado y opciones de la pregunta.
     */
    public String imprimePregunta() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.enunciado).append("\n");
        for (String opcion : this.opciones) {
            stringBuilder.append(opcion).append("\n");
        }
        return stringBuilder.toString();
    }

}
