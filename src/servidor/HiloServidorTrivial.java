package servidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.Semaphore;


/**
 * Los hilos. Manejarán el socket que les pasa el servidor y harán la gestión
 * directa del trivial en sí, permitiendo a los clientes jugar siempre y cuando
 * el Semaphore pasado también por el servidor lo permita.
 * @author Bruno Núñez Manuel   1ºDAM DUAL B
 * @since 26/03/2021
 * @version 1.0
 */
public class HiloServidorTrivial extends Thread {
    Socket socket;
    Semaphore semaphore;

    public HiloServidorTrivial(Socket socket, Semaphore semaphore) {
        this.socket = socket;
        this.semaphore = semaphore;
    }

    /**
     * Si el hilo adquiere un permiso del semáforo, se puede jugar, si
     * no, el cliente tendrá que esperar. Al finalizar el juego, se
     * devuelve el permiso.
     */
    @Override
    public void run() {
        try {
            semaphore.acquire();
            jugar();
            semaphore.release();

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * El core de la app en sí. Aquí sucede el intercambio de la conexión por parte
     * del servidor con cada cliente. Se pide el nombre, se cargan las preguntas de un
     * .txt, se crean los data streams y se hace el intercambio de strings con los clientes.
     * También se establece y envía la puntuación.
     * @throws IOException Si no encuentra el .txt con las preguntas o problemas con los streams.
     */
    private void jugar() throws IOException {
        ArrayList<Pregunta> preguntas =
                new Fichero("src/servidor/preguntas.txt").cargarPreguntas();
        Collections.shuffle(preguntas);
        DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());

        dataOutputStream.writeUTF("\nCuál es tu nombre?");
        String nombre = dataInputStream.readUTF();
        int puntos = 0;
        String respuestaJugador;
        for (Pregunta p : preguntas.subList(0, 5)) {
            dataOutputStream.writeUTF(
                         "\nPuntuación: " + puntos +
                            "\nElige la respuesta correcta [a/b/c/d]" +
                            "\n" + p.imprimePregunta());
            respuestaJugador = dataInputStream.readUTF();
            if (respuestaJugador.equalsIgnoreCase(p.getCorrecta())) {
                puntos++;
            }
        }
        dataOutputStream.writeUTF(
                "\n" + nombre + ", tu puntuación ha sido: " + puntos + "/5\nADIÓS");

        socket.close();
    }

}
